function Get-PartnerPortal {
    <#
    .SYNOPSIS
        A Fast TUI Partner Portal
    .DESCRIPTION
        A Fast TUI Partner Portal
    .FUNCTIONALITY
        TUI Portal
    #>
    [Cmdletbinding()]
    param()

    Begin {
        try {
            if (-not ([System.Management.Automation.PSTypeName]'Kureq.PartnerPortal').Type) {
                Add-Type -Path "$PSScriptRoot\lib\NStack.dll"
                Add-Type -Path "$PSScriptRoot\lib\Kureq.PartnerPortal.dll"
            }

        } catch [System.Reflection.ReflectionTypeLoadException] {
            Write-Output "Message: $($_.Exception.Message)"
            Write-Output "StackTrace: $($_.Exception.StackTrace)"
            Write-Output "LoaderExceptions: $($_.Exception.LoaderExceptions)"
        }
        $Module = "MSOnline"
        if (Get-Module -ListAvailable -Name $Module) {
            Import-Module -Name $Module
        } else {
            Install-Module -Name $Module -Scope CurrentUser -Force
            Import-Module -Name $Module
        }
    } process {
        $MSolCustomers = @{ }
        $UserCredential = Get-Credential
        Connect-MsolService -Credential $UserCredential
        Get-MsolPartnerContract -All | ForEach-Object {
            $val = @{
                Name              = $_.Name
                DefaultDomainName = $_.DefaultDomainName
                TenantId          = $_.TenantId
            }
            $MSolCustomers.Add($_.Name, $val)
        } 2> $null
        $Customers = New-Object 'System.Collections.Generic.Dictionary[String,object]'

        foreach ($c in $MSolCustomers.GetEnumerator()) {
            $val = @{
                'Name'                               = "$($c.Value.Name)"
                'Default Domain'                     = "$($c.Value.DefaultDomainName)"
                'Tenant Id'                          = "$($c.Value.TenantId)"
                'Azure Active Directory'             = "https://aad.portal.azure.com/$($c.Value.DefaultDomainName)"
                'Cloud App Security'                 = "https://portal.cloudappsecurity.com/oauth2/authorize?CTID=$($c.Value.TenantId)"
                'Power Platform'                     = "https://admin.powerplatform.microsoft.com/account/login/$($c.Value.TenantId)"
                'Exchange'                           = "https://outlook.office365.com/ecp/?rfr=Admin_o365&exsvurl=1&delegatedOrg=$($c.Value.DefaultDomainName)"
                'Skype For Business'                 = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=MicrosoftCommunicationsOnline"
                'Office 365 Admin'                   = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
                'MFA Portal'                         = "https://portal.azure.com/$($c.Value.DefaultDomainName)#blade/Microsoft_AAD_IAM/MultifactorAuthenticationMenuBlade/GettingStarted/fromProviders/hasMFALicense/false"
                'Power BI'                           = "https://app.powerbi.com/adminPortal?ctid=$($c.Value.TenantId)"
                'Office 365 Planner'                 = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
                'Intune'                             = "https://portal.azure.com/$($c.Value.DefaultDomainName)/#blade/Microsoft_Intune_DeviceSettings/ExtensionLandingBlade/overview"
                #'SharePoint'                        = "https://???-admin.sharepoint.com/"
                'Sway'                               = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
                'Teams'                              = "https://admin.teams.microsoft.com/?delegatedOrg=$($c.Value.DefaultDomainName)"
                'Windows 10'                         = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
                'Azure Management Portal'            = "https://portal.azure.com/$($c.Value.DefaultDomainName)"
                'Windows Defender ATP'               = "https://securitycenter.windows.com/?tid=$($c.Value.TenantId)"
                'Azure IoT Central'                  = "https://apps.azureiotcentral.com/myapps?tenant=$($c.Value.DefaultDomainName)"
                'Visual Studio Marketplace'          = "https://app.vsaex.visualstudio.com/integrationredirect/cspPartnerSignin?tenantId=$($c.Value.TenantId)&replyto=https://marketplace.visualstudio.com"
                'Manage Visual Studio Subscriptions' = "https://app.vsaex.visualstudio.com/integrationredirect/cspPartnerSignin?tenantId=$($c.Value.TenantId)&replyto=https://manage.visualstudio.com/Subscribers?act_id=2"
            }
            $Customers.Add("$($c.Value.Name)", $val)
        }

        $OldTitle = $host.UI.RawUI.WindowTitle
        $host.UI.RawUI.WindowTitle = "Microsoft Partner TUI Toolkit"
        New-Object -TypeName Kureq.PartnerPortal ($Customers)
        $host.UI.RawUI.WindowTitle = $OldTitle

    }
}

Export-ModuleMember -Function Get-PartnerPortal