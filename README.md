# Partner TUI Toolkit - A TUI for accessing MS Partner Resources
**Based on Gui.CS and MSOnline**

![Sample app](https://gitlab.com/kureq/partnertuitoolkit/-/raw/master/img/example.gif)

## Features

* **Quickly access CSP customer without going through the sluggish Partner Portal**
* **Totally Rad Terminal UI**

## Installing

Use `Install-Module` to install the `PartnerTUIToolkit` Powershell Module: https://www.powershellgallery.com/packages/PartnerTUIToolkit
```` powershell
Install-Module -Name PartnerTUIToolkit
````

## Usage

Currently the Module only exposes a Single Function `Get-PartnerPortal`.\
The function will install the required resources, `MSOnline`, and prompt for user credentials. \
After successful authentication the Function presents a Terminal UI with three views:
* Filter (Top-Left): Filter through your client tenants, currently it only supports plain text.
* Tenants (Bottom-Left): A list of the filtered tenants, if no filter is applied it will display all tenants.
* Current-Tenant (Right): A list of possible actions on the currently selected Tenant.

### Navigation
* `TAB` change view
* `ENTER` Goto Currently Selected Client, or complete action in Current-Tenant View.
* `CTRL-Q` Exit the Application.
* `Arrow Keys` Navigate the lists in the views.

## Running and Building
* Windows - Build and run using the .NET SDK command line tools (`dotnet build` in the source directory) or open `PartnerPortal.sln` with Visual Studio.
