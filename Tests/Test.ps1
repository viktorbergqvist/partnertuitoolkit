
try {
    if (-not ([System.Management.Automation.PSTypeName]'Kureq.PartnerPortal').Type) {
        Add-Type -Path "$PSScriptRoot\..\PartnerTUIToolkit\lib\NStack.dll"
        #Add-Type -Path "$PSScriptRoot\lib\Terminal.Gui.dll"
        Add-Type -Path "$PSScriptRoot\..\PartnerTUIToolkit\lib\Kureq.PartnerPortal.dll"
        #$Assemblies = ("System.Core", "NStack", "netstandard", "$PSScriptRoot\lib\Terminal.Gui.dll")
        #$Source = Get-Content -Path "Program.cs" -Raw
        #Add-Type -ReferencedAssemblies $Assemblies -TypeDefinition $Source -Language CSharp
    }

} catch [System.Reflection.ReflectionTypeLoadException] {
    Write-Host "Message: $($_.Exception.Message)"
    Write-Host "StackTrace: $($_.Exception.StackTrace)"
    Write-Host "LoaderExceptions: $($_.Exception.LoaderExceptions)"
}
$MSolCustomers = @{ }

for ($i = 1; $i -le 200; $i++) {
    $val = @{
        Name              = "client $i"
        DefaultDomainName = "client$($i).onmicrosoft.com"
        TenantId          = New-Guid
    }
    $MSolCustomers.Add("test $($i)", $val)
}

$Customers = New-Object 'System.Collections.Generic.Dictionary[String,object]'
foreach ($c in $MSolCustomers.GetEnumerator()) {
    $val = @{
        'Name'                               = "$($c.Value.Name)"
        'Default Domain'                     = "$($c.Value.DefaultDomainName)"
        'Tenant Id'                          = "$($c.Value.TenantId)"
        'Azure Active Directory'             = "https://aad.portal.azure.com/$($c.Value.DefaultDomainName)"
        'Cloud App Security'                 = "https://portal.cloudappsecurity.com/oauth2/authorize?CTID=$($c.Value.TenantId)"
        'Power Platform'                     = "https://admin.powerplatform.microsoft.com/account/login/$($c.Value.TenantId)"
        'Exchange'                           = "https://outlook.office365.com/ecp/?rfr=Admin_o365&exsvurl=1&delegatedOrg=$($c.Value.DefaultDomainName)"
        'Skype For Business'                 = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=MicrosoftCommunicationsOnline"
        'Office 365 Admin'                   = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
        'MFA Portal'                         = "https://portal.azure.com/$($c.Value.DefaultDomainName)#blade/Microsoft_AAD_IAM/MultifactorAuthenticationMenuBlade/GettingStarted/fromProviders/hasMFALicense/false"
        'Power BI'                           = "https://app.powerbi.com/adminPortal?ctid=$($c.Value.TenantId)"
        'Office 365 Planner'                 = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
        'Intune'                             = "https://portal.azure.com/$($c.Value.DefaultDomainName)/#blade/Microsoft_Intune_DeviceSettings/ExtensionLandingBlade/overview"
        #'SharePoint'                         = "https://c4-admin.sharepoint.com/"
        'Sway'                               = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
        'Teams'                              = "https://admin.teams.microsoft.com/?delegatedOrg=$($c.Value.DefaultDomainName)"
        'Windows 10'                         = "https://portal.office.com/Partner/BeginClientSession.aspx?CTID=$($c.Value.TenantId)&CSDEST=o365admincenter"
        'Azure Management Portal'            = "https://portal.azure.com/$($c.Value.DefaultDomainName)"
        'Windows Defender ATP'               = "https://securitycenter.windows.com/?tid=$($c.Value.TenantId)"
        'Azure IoT Central'                  = "https://apps.azureiotcentral.com/myapps?tenant=$($c.Value.DefaultDomainName)"
        'Visual Studio Marketplace'          = "https://app.vsaex.visualstudio.com/integrationredirect/cspPartnerSignin?tenantId=$($c.Value.TenantId)&replyto=https://marketplace.visualstudio.com"
        'Manage Visual Studio Subscriptions' = "https://app.vsaex.visualstudio.com/integrationredirect/cspPartnerSignin?tenantId=$($c.Value.TenantId)&replyto=https://manage.visualstudio.com/Subscribers?act_id=2"
    }
    $Customers.Add("$($c.Value.Name)", $val)
}
<#$params = New-Object 'System.Collections.Generic.Dictionary[String, object]'
1..100 | % {
    $params.Add("$_ $(Get-Random -Minimum 2 -Maximum 200)", (Get-Random -Minimum 2 -Maximum 200)) 2> $null
}#>
$OldTitle = $host.UI.RawUI.WindowTitle
$host.UI.RawUI.WindowTitle = "Microsoft Partner TUI Toolkit"
New-Object -TypeName Kureq.PartnerPortal ($Customers)
$host.UI.RawUI.WindowTitle = $OldTitle
#Invoke-PowerShell
#Remove-Variable -Name TUI

#Connect-MsolService
#$Customers = Get-MsolPartnerContract -All
#$Customers | ForEach-Object { "$($_.TenantId) | $($_.DefaultDomainName)" } 2> $null
#$z = New-Object Terminal.Gui.MenuBar ("")


#Write-Output "$x was selected as output"