﻿using NStack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Terminal.Gui;

namespace Kureq
{
    public class PartnerPortal : IDisposable
    {
        private static Toplevel _top;
        private static Window _left;
        private static Window _right;
        private static Window _search;
        private static ListView _tenantsListView;
        private static ListView _currentTenantListView;
        private static StatusBar _statusBar;

        private TextField _searchField;
        private static Hashtable _tenants;
        private static List<string> _tenantList;
        private static List<string> _filterList;
        private static StringCompare _compare;

        public PartnerPortal(Hashtable _table)
        {
            _tenants = _table;
            _compare = new StringCompare();

            Application.Init();

            _top = Application.Top;
            _top.Running = true;
            _top.KeyDown += KeyDownHandler;

            _search = new Window("Microsoft Partner TUI Toolkit")
            {
                X = 0,
                Y = 0,
                Width = 45,
                Height = 4,
                CanFocus = false
            };

            _right = new Window("Test")
            {
                X = 45,
                Y = 0, // for menu
                Width = Dim.Fill(),
                Height = Dim.Fill(),
                CanFocus = false,
            };

            _left = new Window("Tenants")
            {
                X = 0,
                Y = 4,
                Width = 45,
                Height = Dim.Fill() -1,
                CanFocus = false,

            };

            _tenantList = _table.Keys.OfType<string>().ToList();
            _tenantList.Sort(_compare);
            _filterList = _tenantList;

            _tenantsListView = new ListView(_filterList)
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(0),
                Height = Dim.Fill(0),
                AllowsMarking = false,
                CanFocus = true,
            };

            _tenantsListView.SelectedChanged += (o, a) =>
            {
                ChangeCurrentTenantsView();
            };

            _tenantsListView.OpenSelectedItem += (o, a) =>
            {
                ChangeCurrentTenantsView();
                _top.SetFocus(_right);
            };

            _left.Add(_tenantsListView);
            _currentTenantListView = new ListView()
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(0),
                Height = Dim.Fill(0),
                AllowsMarking = false,
                CanFocus = true,
            };
            _currentTenantListView.OpenSelectedItem += CurrentTenantListView_OpenSelectedItem;
            _right.Add(_currentTenantListView);
            ResetSelection(_tenantsListView);

            _searchField = new TextField(string.Empty)
            {
                X = 0,
                Y = 1,
                CanFocus = true,
                Width = Dim.Fill()
            };

            _searchField.Changed += (object sender, ustring e) =>
            {
                if (!e.IsEmpty)
                {
                    string filterText = (sender as TextField)?.Text?.ToString();
                    _filterList = TenantsListDataSource.FilterData(_tenantList, filterText);
                    _tenantsListView.Source = new TenantsListDataSource(_filterList);
                }
            };

            var searchLabel = new Label("Filter")
            {
                X = 0,
                Y = 0
            };

            _search.Add(searchLabel, _searchField);

            _statusBar = new StatusBar(new StatusItem[] {
				new StatusItem(Key.ControlQ, "~CTRL-Q~ Quit", () => {
                    KillMe();
                })
            });

            /*var menu = new MenuBar(new MenuBarItem[] {
                new MenuBarItem ("_File", new MenuItem [] {
                    new MenuItem ("_Quit", "", () => {KillMe(); })
                })
            });*/

            _top.Add(_search);
            _top.Add(_left);
            _top.Add(_right);
            _top.Add(_statusBar);
            //_top.Add(menu);

            Application.Run();
        }

        private static void CurrentTenantListView_OpenSelectedItem(object sender, EventArgs e)
        {
            ((TenantsListDataSource)_currentTenantListView.Source).ExecuteLink();
        }
        public void KillMe()
        {
            //_top.Running = false;
            Application.RequestStop();
        }
        public void Dispose()
        {
            Console.Write("\u001b[?1h");
        }
        public void ResetSelection(ListView l)
        {
            //l.SelectedItem = 0;
            ChangeCurrentTenantsView();
        }

        private void KeyDownHandler(object sender, View.KeyEventEventArgs a)
        {

            if (a.KeyEvent.Key == Key.Tab)
            {
                if (_top.MostFocused == _tenantsListView)
                {
                    _top.SetFocus(_right);
                }

                else if (_top.MostFocused == _currentTenantListView)
                {
                    _top.SetFocus(_search);
                }
                else
                {
                    _top.SetFocus(_left);
                    ResetSelection(_tenantsListView);
                }

            }
            else if (a.KeyEvent.Key == Key.BackTab)
            {
                if (_top.MostFocused == _tenantsListView)
                {
                    _top.SetFocus(_search);
                }
                else if (_top.MostFocused == _currentTenantListView)
                {
                    _top.SetFocus(_left);
                    ResetSelection(_tenantsListView);
                }
                else
                {
                    _top.SetFocus(_right);
                }

            } else if (a.KeyEvent.Key == Key.Enter)
            {
                if (_top.MostFocused == _tenantsListView)
                {
                    _top.SetFocus(_search);
                }
                else if (_top.MostFocused == _search)
                {
                    _top.SetFocus(_left);
                    ResetSelection(_tenantsListView);
                }
            }
        }

        private void ChangeCurrentTenantsView()
        {
            Log("is Null? " + (_tenantsListView.Source != null));
            Log("Large ? " + (_tenantsListView.Source.Count));
            if (_tenantsListView.Source != null && _tenantsListView.Source.Count > 0)
            {
                var i = _filterList[_tenantsListView.SelectedItem];
                if (i != null)
                {
                    Hashtable item = (Hashtable)_tenants[i];
                    _currentTenantListView.Source = new TenantsListDataSource(item);
                    var _title = _filterList[_tenantsListView.SelectedItem];
                    if (string.IsNullOrEmpty(_title))
                    {
                        _right.Title = "HELLO";
                    }
                    else
                    {
                        _right.Title = $"{_title}";
                    }
                }
            }
        }

        internal class TenantsListDataSource : IListDataSource
        {
            public List<String> Src { get; set; }
            private Hashtable links;
            public bool IsMarked(int item) => false;

            public int Count => Src.Count;
            private int _length = -1;

            public TenantsListDataSource(List<string> itemList)
            {
                // this._length = itemList.OrderByDescending(t => t.Length).FirstOrDefault().Length;
                Src = itemList;
            }
            public TenantsListDataSource(Hashtable _table)
            {
                List<string> _v = new List<string>();
                links = new Hashtable();
                foreach (string key in _table.Keys)
                {
                        _v.Add(key);
                }
                _v.Sort(_compare);

                for (var i = 0; i < _v.Count; i++)
                {
                    links.Add(_v[i], _table[_v[i]]);
                    if (!_table[_v[i]].ToString().Contains("http"))
                    {
                        String s = _v[i];
                        _v.RemoveAt(i);
                        _v.Insert(0, s);
                    }
                }
                this._length = _v.OrderByDescending(t => t.Length).FirstOrDefault().Length;

                Src = _v;
            }
            public void ExecuteLink() //that little bitch
            {
                if (links != null)
                {
                    string uriName = (string)links[Src[_currentTenantListView.SelectedItem]];
                    if (uriName.Contains("http"))
                    {
                        System.Diagnostics.Process.Start(uriName);
                    }
                }
            }
            public void Render(ListView container, ConsoleDriver driver, bool selected, int item, int col, int line, int width)
            {
                container.Move(col, line);
                var t = Src[item];
                if (t == null)
                {
                    RenderUstr(driver, ustring.Make(""), col, line, width);
                }
                else
                {
                    if (t is string)
                    {
                        if (links != null)
                        {
                            string v = (string)links[t];
                            if (v != null)
                            {
                                if (!v.Contains("http"))
                                {
                                    var s = String.Format(String.Format("{{0,{0}}}", _length), t);
                                    RenderUstr(driver, $"   {s}    {v}", col, line, width);
                                    return;
                                }
                                else
                                {
                                    var s = String.Format(String.Format("{{0,{0}}}", _length), t);
                                    RenderUstr(driver, $"   {s}    -> Open Website", col, line, width);
                                    return;
                                }
                            }

                        }

                    }
                    RenderUstr(driver, t.ToString(), col, line, width);
                }
            }
            public static List<string> FilterData(List<string> list, string filter)
            {
                var items = new List<string>();
                if (string.IsNullOrEmpty(filter))
                {
                    filter = ".*";
                }

                foreach (string gvr in list)
                {
                    if (Regex.IsMatch(gvr, filter, RegexOptions.IgnoreCase))
                    {
                        items.Add(gvr);
                    }
                }
                return items;
            }

            public void SetMark(int item, bool value)
            {
            }

            private void RenderUstr(ConsoleDriver driver, ustring ustr, int col, int line, int width)
            {
                int used = 0;
                int index = 0;
                while (index < ustr.Length)
                {
                    (var rune, var size) = Utf8.DecodeRune(ustr, index, index - ustr.Length);
                    var count = Rune.ColumnWidth(rune);
                    if (used + count >= width) break;
                    driver.AddRune(rune);
                    used += count;
                    index += size;
                }

                while (used < width)
                {
                    driver.AddRune(' ');
                    used++;
                }
            }

            public IList ToList()
            {
                return Src;
            }

        }

        public static void Log(String logMessage)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()} : {logMessage}");
            }

        }

        public class StringCompare : IComparer<string>
        {
            //from https://stackoverflow.com/a/9989709
            public int Compare(string x, string y)
            {
                var regex = new Regex("(\\d+)");

                // run the regex on both strings
                var xRegexResult = regex.Match(x);
                var yRegexResult = regex.Match(y);

                // check if they are both numbers
                if (xRegexResult.Success && yRegexResult.Success)
                {
                    return int.Parse(xRegexResult.Groups[1].Value).CompareTo(int.Parse(yRegexResult.Groups[1].Value));
                }

                // otherwise return as string comparison
                return x.CompareTo(y);
            }

        }
    }
}